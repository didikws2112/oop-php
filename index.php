<?php

require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');

$animal = new Animal("Shaun");

echo "Name : ".$animal->name."<br>";
echo "legs : ".$animal->legs."<br>";
echo "cold_blooded : ".$animal->cold_blooded."<br><br>";

$frog = new Frog("buduk");

echo "Name : ".$frog->name."<br>";
echo "legs : ".$frog->legs."<br>";
echo "cold_blooded : ".$frog->cold_blooded."<br>";
echo $frog->jump()."<br><br>";


$ape = new Ape("kera sakti");

echo "Name : ".$ape->name."<br>";
echo "legs : ".$ape->legs."<br>";
echo "cold_blooded : ".$ape->cold_blooded."<br>";
echo $ape->yell();

?>